import React, {Component} from 'react';

import Wrapper from '../../../hoc/Wrapper/Wrapper';
import Button from '../../../components/UI/Button/Button';

class OrderSummary extends Component {

    render() {
        const ingredientSummary = Object.keys(this.props.ingredients)
            .map(igKey => {
                return (
                    <li key={igKey}>
                        <span style={{textTransform: 'capitalise'}}>{igKey}: {this.props.ingredients[igKey]}</span>
                    </li>
                );
            });

        return (
            <Wrapper>
                <h1>Your order</h1>
                <p>Burger:</p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p><strong>Total price: {this.props.price.toFixed(2)}</strong></p>
                <p>Go to checkout</p>
                <Button buttonType={'Danger'}
                    clicked={this.props.purchaseCancelled}>CANCEL</Button>
                <Button buttonType={'Success'}
                    clicked={this.props.purchaseContinued}>CONTINUE</Button>
            </Wrapper>
        );
    }

};

export default OrderSummary;